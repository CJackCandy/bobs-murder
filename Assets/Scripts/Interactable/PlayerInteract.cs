using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInteract : MonoBehaviour
{
    [Header("Controls")] 
    [SerializeField] KeyCode _interactKey = KeyCode.Mouse0;
    
    [Header("Functional Options")] 
    [SerializeField] bool _canInteract = true;

    [SerializeField] Camera _camera;
    
    [Header("Interaction")]
    [SerializeField] float _interactionDistance = 10f;
    [SerializeField] float _searchDistance = 5f;
    [SerializeField] LayerMask _interactionLayer;
    // [SerializeField] LayerMask _blockingLayer;
    // [SerializeField] Image _crossHair;
    [SerializeField] GameObject _searchPrompt;
    [SerializeField] TMP_Text _searching;
    [SerializeField] Image _spinner;
    [SerializeField] float _searchTime = 2f;
    Interactable _currentInteractable;
    float _searchTimer;
    bool _searchStarted = false;
    bool _searchCompleted = false;

    Vector3 _fireDirection;
    Vector3 _firePoint;
    

    void Update()
    {
        // _searching.gameObject.SetActive(false);
        
        _fireDirection = transform.TransformDirection(Vector3.forward) * _interactionDistance;
        _firePoint = _camera.transform.position;
        // Debug the ray out in the editor:
        // Debug.DrawRay(_firePoint, _fireDirection, Color.green);
        
        if (_canInteract)
        {
            HandleInteractionCheck();
            HandleInteractionInput();
        }
    }

    public void ResetObjects()
    {
        _searchStarted = false;
        _searchCompleted = false;
        
    }

    private void HandleInteractionCheck()
    {
        Debug.DrawRay(_firePoint, _fireDirection, Color.gray);

        if (Physics.Raycast(_camera.transform.position, _fireDirection, out RaycastHit hit, _interactionDistance)) //, _interactionLayer))
        {
            if (hit.transform.CompareTag("Searchable"))
            {
                Debug.DrawRay(_firePoint, _fireDirection, Color.green);
                hit.collider.TryGetComponent<Interactable>(out _currentInteractable);

                if (_currentInteractable) 
                {
                    if (_currentInteractable.SearchCompleted)
                        return;
                    float distance = Vector3.Distance(transform.position, hit.transform.position);
                    // Debug.Log("distance = " + distance);
                    if (distance < _searchDistance)
                    {
                        
                        _searchPrompt.SetActive(true);
                    }

                    _currentInteractable.OnFocus();
                }
            }
            else
            {
                if (_currentInteractable)
                {
                    _currentInteractable.OnLoseFocus();
                    _currentInteractable = null;
                }
            }
        }
        else
        {
            if (_currentInteractable)
            {
                _currentInteractable.OnLoseFocus();
                _currentInteractable = null;
            }
        }

        
        // if (Physics.Raycast (Camera.main.transform.position, _fireDirection, out RaycastHit hit, _interactionDistance, _interactionLayer))
        // {
        //     if (hit.collider.gameObject.layer == 6 && (_currentInteractable == null || hit.collider.gameObject.GetInstanceID() != _currentInteractable.GetInstanceID()))
        //     {
        //         hit.collider.TryGetComponent<Interactable>(out _currentInteractable);
        //
        //         if (_currentInteractable)
        //         {
        //             _crossHair.color = Color.red;
        //             _currentInteractable.OnFocus();
        //         }
        //     }
        //     else if (_currentInteractable)
        //     {
        //         _crossHair.color = Color.white;
        //         _currentInteractable.OnLoseFocus();
        //         _currentInteractable = null;
        //     }
        // }
        // else
        // {
        //     _crossHair.color = Color.white;
        //     if (_currentInteractable)
        //     {
        //         _currentInteractable.OnLoseFocus();
        //         _currentInteractable = null;
        //     }
        // }
    }

    private void HandleInteractionInput()
    {
        if (_currentInteractable == null)
            return;
        
        if (_currentInteractable.SearchCompleted)
            return;
        
        if (Input.GetKeyDown(_interactKey) && _currentInteractable != null &&
            Physics.Raycast(_camera.transform.position, _fireDirection, out RaycastHit hit, _searchDistance,
                _interactionLayer))
        {
            _searchStarted = true;
            _searchCompleted = false;
            _searchTimer = 0f;
        }

        if (_searchStarted)
        {
            // _crossHair.color = Color.green;
            _searching.gameObject.SetActive(true);
            _searchPrompt.SetActive(false);
            
            _searchTimer += Time.deltaTime;

            float pct = _searchTimer / _searchTime;
            _spinner.fillAmount = pct;
            
            if (_searchTimer >= _searchTime)
            {
                _searching.gameObject.SetActive(false);
                _searchStarted = false;
                _searchCompleted = true;
                _spinner.fillAmount = 0f;
                
                if (_currentInteractable)
                    _currentInteractable.OnInteract();
            } 
        }
        
        if (Input.GetKeyUp(_interactKey))
        {
            _searching.gameObject.SetActive(false);
            _searchStarted = false;
            _spinner.fillAmount = 0f;
        }
    }
}
