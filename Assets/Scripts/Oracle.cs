using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oracle : MonoBehaviour
{
    [SerializeField] GameManager _gameManager;
    [SerializeField] GameObject _findMoreClues;
    [SerializeField] GameObject _allCluesFound;
    [SerializeField] GameObject _questDialog;
    [SerializeField] GameObject _gameOverPopup;

    bool _introPlayed = false;

    public void InitOracle()
    {
        _introPlayed = false;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<FirstPersonMovement>())
        {
            if (!_introPlayed)
            {
                _questDialog.SetActive(true);
                _introPlayed = true;
                return;
            }
            
            if (_gameManager.Score >= 3)
            {
                // print("Oracle: You may now enter the After Life!");
                _allCluesFound.SetActive(true);
                StartCoroutine("FadeToExit");
            }
            else
            {
                // print("Oracle: You must find the 3 clues!");
                _findMoreClues.SetActive(true);
            }
            
        }
    }

    IEnumerator FadeToExit()
    {
        yield return new WaitForSeconds(2);
        _gameOverPopup.SetActive(true);
        _gameManager.GetComponent<CursorLockUnLock>().UnlockCursor();
    }

    void OnTriggerExit(Collider other)
    {
        _questDialog.SetActive(false);
        _findMoreClues.SetActive(false);
        _allCluesFound.SetActive(false);
    }
}
