using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class OracleAnimate : MonoBehaviour
{

    [SerializeField] float rotationSpeed = 50;
    [SerializeField] float moveDistance = 3f;
    [SerializeField] float moveSpeed = 2f;
    void Update()
    {
        Animate();
    }

    void Animate()
    {
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);

        Vector3 newPos = transform.position;
        newPos.y = newPos.y + moveDistance; // moveDistance * (Mathf.Sin(Time.time * moveSpeed - (Mathf.PI / 10f))); // + moveDistance;
        transform.position = newPos;
    }
}
