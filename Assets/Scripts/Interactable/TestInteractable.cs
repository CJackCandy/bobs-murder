using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class TestInteractable : Interactable
{
    [SerializeField] GameObject _clue;
    public override void OnInteract()
    {
        print("Interacting with " + gameObject.name);
        _clue.SetActive(true);
        // gameObject.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }

    public override void OnFocus()
    {
        GetComponent<Outline>().enabled = true;
        print("Looking at " + gameObject.name);
    }

    public override void OnLoseFocus()
    {
        GetComponent<Outline>().enabled = false;
        _clue.SetActive(false);
        print("Stopped looking at " + gameObject.name);
    }
}
