using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class UI_LookAtPlayer : MonoBehaviour
{
    [SerializeField] GameObject _player;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Camera.main.transform);
        transform.Rotate(0, 180, 0);
    }
}
