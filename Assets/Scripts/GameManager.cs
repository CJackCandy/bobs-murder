using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    int _score;
    [SerializeField] TMP_Text _scoreText;
    [SerializeField] TMP_Text _clue1;
    [SerializeField] TMP_Text _clue2;
    [SerializeField] TMP_Text _clue3;
    [SerializeField] GameObject[] _searchableObjects;
    [SerializeField] GameObject[] _clues;
    [SerializeField] GameObject[] _clueHUDArray;
    [SerializeField] GameObject _oracle;

    List<int> _searchableObjectsIndexList = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

    String[] _room = new string[] { "kitchen", "living room", "bed room", "dining room", "attic", "basement", "laundry room" };
    String[] _weapon = new string[] { "knife", "gun", "pepper mill", "wrench", "trophy", "fireplace poker", "rock", "machete", "katana"};
    String[] _killer = new string[] { "your cousin, Joey", "the cable guy", "the plumber", "a green alien", "the gardner", "a ninja", "a birthday clown"};
    
    public int Score => _score;

    void Awake()
    {
        Init();
    }

    public void Init()
    {
        _score = 0;
        
        // Reset searchable objects 
        for (int i = 0; i < _searchableObjects.Length; i++)
        {
            _searchableObjects[i].GetComponent<SearchInteractable>().SearchCompleted = false;
        }

        // Initialize HUD elements
        _scoreText.text = "Facts Found: 0";

        var j = 1;
        for (int i = 0; i < 3; i++)
        {
            _clueHUDArray[i].GetComponent<TMP_Text>().text = "Fact " + j + ":";
            j++;
        }
        
        // Initialize Clues UI elements
        _clue1.GetComponent<TMP_Text>().text = "Fact: Your were killed in the ";
        _clue2.GetComponent<TMP_Text>().text = "Fact: You were killed with a ";
        _clue3.GetComponent<TMP_Text>().text = "Fact: The Killer is ";
        
        // _oracle.GetComponent<Oracle>().InitOracle();

        // Assign random clues
        var index = Random.Range(0, _room.Length);
        _clue1.text = _clue1.text + " " + _room[index];
        
        index = Random.Range(0, _weapon.Length);
        _clue2.text = _clue2.text + " " +  _weapon[index];
        
        index = Random.Range(0, _killer.Length);
        _clue3.text = _clue3.text + " " + _killer[index];

        // _searchableObjects[0].GetComponent<SearchInteractable>()._clue = _clues[1];
        
        // Initialize all seachable objects
        for (int i = 0; i < _searchableObjects.Length; i++)
        {
            _searchableObjects[i].GetComponent<SearchInteractable>().Clue = _clues[3];
            _searchableObjects[i].GetComponent<SearchInteractable>().ClueHUDText = null;
        }
        
        // Shuffle searchable objects index list
        Shuffle(_searchableObjectsIndexList);
        print("shuffled list => " + string.Join(", ", _searchableObjectsIndexList));
        
        // Assign clues randomly using shuffled list- only 3 searchable objects will contain clues
        for (int i = 0; i < _clues.Length - 1; i++)
        {
            var idx = _searchableObjectsIndexList[i];
            // print("idx = " + idx);
            _searchableObjects[idx].GetComponent<SearchInteractable>().Clue = _clues[i];
            if ( i < 3)
                _searchableObjects[idx].GetComponent<SearchInteractable>().ClueHUDText = _clueHUDArray[i];
        }
    }

    void Shuffle<T>(List<T> inputList)
    {
        for (int i = 0; i < inputList.Count; i++)
        {
            T temp = inputList[i];
            int rand = Random.Range(i, inputList.Count);
            inputList[i] = inputList[rand];
            inputList[rand] = temp;
        }
    }

    public void UpdateScore()
    {
        _score++;

        _scoreText.text = "Facts Found: " + _score;
    }
}
