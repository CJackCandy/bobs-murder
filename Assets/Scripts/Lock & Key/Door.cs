using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] KeyType doorType;
    [SerializeField] bool removeUsedKey = true;
    [SerializeField] Canvas _canvas;
    [SerializeField] AudioSource _doorSound;

    Animator anim;
    bool _doorIsClosed = true;

    void Awake()
    {
        anim = GetComponentInChildren<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        Inventory playerInventory = other.GetComponent<Inventory>();

        if (playerInventory == null) { return; }
        if (anim == null) { return; }

        if (playerInventory.IsHoldingKey(doorType) && _doorIsClosed)
        {
            _doorSound.PlayOneShot(_doorSound.clip, 0.5f);
            anim.SetBool("isOpen", true);
            if (removeUsedKey)
            {
                playerInventory.RemoveKey(doorType);
            }

            _doorIsClosed = false;
        }
        else if (_doorIsClosed)
        {
            _canvas.GameObject().SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        _canvas.GameObject().SetActive(false);
    }
}
