using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SearchInteractable : Interactable
{
    [SerializeField] GameObject _searchPrompt;
    public GameObject Clue;
    public GameObject ClueHUDText;

    
    public override void OnInteract()
    {
        if (SearchCompleted)
            return;
        
        _searchPrompt.SetActive(false);
        Clue.SetActive(true);
        SearchCompleted = true;

        if (Clue.CompareTag("Clue"))
        {
            // print("Clue Found");
            var gameManager = GameObject.FindObjectOfType<GameManager>();
            gameManager.UpdateScore();

            ClueHUDText.GetComponent<TMP_Text>().text = Clue.GetComponent<TMP_Text>().text;
            
        }
    }

    public override void OnFocus()
    {
        if (SearchCompleted)
            return;
        
        // _searchPrompt.SetActive(true);
        GetComponent<Outline>().enabled = true;
    }

    public override void OnLoseFocus()
    {
        _searchPrompt.SetActive(false);
        GetComponent<Outline>().enabled = false;
        Clue.SetActive(false);
    }
}
